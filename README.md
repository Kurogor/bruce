# Bruce

This repository wants to port the possibility to mess with github contribution graph to gitlab\
It's goal is to use the graph to display a simple 51*7 pixel image as a banner (hence the repository name)

![A small example of banner](img/example.png)

## Usage

* Install the dependencies with `pip install -r requirements.txt`
* Edit `values.yaml` file to set your values
* If you don't already have a comment on the gitlab, create one
* Lanch banner.py
* Save the command logged to remove this activity later
* Check your gitlab profile

## `values.yaml` file

The values.yaml file is as follow
```yaml
postgres:
  host: "postgres.gitlab.example.org"
  port: 5432
  database: "gitlabhq_production"
  user: "gitlab"
  password: "password"

pixelart:
  user: ma
  drawing: |-
    ..####.......#####............####............##.......####..
    .######........#..#..........######.........######....######.
    .##.#.#........#...##........##.#.#...........####....#.#.##.
    ########..#.....#....#...#..########.#....#....####..########
    ########.......###..###.....########..........####...########
    ########.......###..###.....########........######...########
    ##.##.##.......##....#......##.##.##..........##.....##.##.##
```

`postgres.host`: the url of the database\
`postgres.port`: the port of the database (default: 5432)\
`postgres.database`: the name of the database used by gitlab (default: gitlabhq_production)\
`postgres.user`: username used by gitlab on the database (default: gitlab)\
`postgres.password`: The password used by gitlab for the database\

`pixel-art.user`: the username of the user you want to edit the graph\
`pixel-art.drawing`: the banner to draw. `#` char means a dot, other chars are left blank\

## How it works

To create fake events, Bruce duplicates an existing comment event in the database.\
This way, only the `events` database is modified and reversing it is pretty fast forward.\
The only downside is that a comment must be made before by the user\

## Disclaimer

/!\ While github contributions graph is based on commits, here we modify the gitlab database directly.\
I am not responsible in any way of the repercussions it could have on your gitlab instance\
Use at your own risk on your private gitlab instances
