FROM ubuntu:20.04

WORKDIR /usr/src/app

RUN apt update && apt install -y \
        python3-pil \
        python3-yaml \
        python3-tz \
        python3-psycopg2 \
        && rm -rf /var/lib/apt/lists

# RUN pip3 install --no-cache-dir -r requirements.txt

COPY src src
COPY values.yaml values.yaml

ENTRYPOINT ["python3", "src/bruce.py"]
