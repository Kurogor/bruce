#!/usr/bin/env python3

import datetime
import os
import psycopg2
import psycopg2.extras
import pytz
import sys

import config as conf
import database_handler as db


SUCCESS_MESSAGE = """To remove created values, run
    DELETE FROM events WHERE id>{before_id} AND id<={after_id} AND project_id={project_id} AND author_id={author_id} AND target_id={target_type} AND action={action} AND target_type='{target_type}';
or relaunch the script overiding the configuration values as follow:

pixelart:
  user: {user}
  draw: false
  before_id: {before_id}
  after_id: {after_id}
  project_id: {project_id}
  target_id: {target_id}
  action: {action}
  target_type: "{target_type}"
"""

##  Draws a banner based on an image duplicating a specific comment
#
#   @param cur Cursor for the postgres connection
#   @param image List of string lines for the image. `#` char is a dot, other chars are left blank
#   @param comment The comment event duplicated to generate events
def draw_banner(cur, image, comment):
    before_id = db.get_last_id(cur)
    width = max([len(line) for line in image])

    day_delta = datetime.timedelta(days=1)
    today = pytz.utc.localize(datetime.datetime.now().replace(
        hour=12, minute=0, second=0, microsecond=0))

    first_day = today - datetime.timedelta(weeks=51)

    while (first_day.weekday() != 6):
        first_day -= day_delta

    for x in range(width):
        for y in range(min(len(image), 7)):
            if x < len(image[y]) and image[y][x] == '#':
                db.insert_event(cur, comment[0], comment[1], comment[2], comment[3], comment[4], first_day)
            first_day += day_delta

    after_id = db.get_last_id(cur)
    print(SUCCESS_MESSAGE.format(before_id=str(before_id), after_id=str(after_id), user=pixart_config.get('user'),
        project_id=comment[0], author_id=comment[1], target_id=comment[2], action=comment[3], target_type=comment[4]))




if __name__ == '__main__':
    data_config = conf.get_config('postgres')
    pixart_config = conf.get_config('pixelart')

    data_env = {key: os.environ.get(key.upper()) for key in ['host', 'port', 'database', 'user', 'password'] if os.environ.get(key.upper()) is not None}
    data_config.update(data_env)

    pixart_env = {key: os.environ.get('PIXART_' + key.upper()) for key in ['user', 'drawing', 'before_id', 'after_id', 'project_id', 'target_id', 'action', 'target_type'] if os.environ.get('PIXART_' + key.upper()) is not None}
    pixart_config.update(pixart_env)

    if os.environ.get('PIXART_DRAW') is not None:
        if os.environ.get('PIXART_DRAW') == 'false':
            pixart_config['draw'] = False
        else:
            pixart_config['draw'] = True

    conn = None
    try:
        conn = psycopg2.connect(**data_config)
        cur = conn.cursor()

        user_id = db.database_verification(cur, data_config.get('database'), pixart_config.get('user'))

        print('Your user id is', user_id)

        comment = db.get_user_random_comment(cur, user_id)

        if pixart_config.get('draw', True):
            draw_banner(cur, pixart_config.get('drawing').split('\n'), comment)
        else:
            params = ['before_id', 'after_id', 'project_id', 'target_id', 'action', 'target_type']
            for param in params:
                if pixart_config.get(param) is None:
                    raise RuntimeError("ERROR: Param " + param + " isn't defined for suppression")

            params = ['before_id', 'after_id', 'project_id', 'target_id', 'action', 'target_type']
            delete_params = {key: pixart_config.get(key) for key in params}

            db.delete_drawing(cur, user_id, **delete_params)

        conn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
