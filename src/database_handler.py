import psycopg2
import psycopg2.extras
import datetime
import sys

import config as conf

CREATED_AT = datetime.datetime.fromtimestamp(1)

##  Verifies the database contains the events and users tables and that the user exists
#
#   @param cur Cursor for the postgres connection
#   @param database Name of the database
#   @param user Name of the user
#   @return int Id of the user
#
#   @error Throws a RuntimeError if a database is missing or if the user doesn't exists
def database_verification(cur, database, user):
    if not (table_exists(cur, database, 'events') and \
            table_exists(cur, database, 'users')):
        raise RuntimeError("ERROR: One database is missing")

    user_id = get_user_id(cur, user)
    if user_id is None:
        raise RuntimeError("ERROR: User " + user + " does not exist")
    return user_id

##  Verify if a specific table exists in the database
#
#   @param cur Cursor for the postgres connection
#   @param database Name of the database
#   @param table_name Name of the table
#   @return bool True if the table exists
def table_exists(cur, database, table_name):
    cur.execute("""
        SELECT EXISTS(SELECT 1 FROM information_schema.tables
            WHERE table_catalog = %s AND
            table_schema = 'public' AND
            table_name = %s)
            """, (database, table_name))
    answer = cur.fetchall()
    return answer[0][0]


##  Gets the user id based on the username
#
#   @param cur Cursor for the postgres connection
#   @param username Name of the user
#   @return int Id of the user if it exists, None otherwise
def get_user_id(cur, username):
    cur.execute("""
        SELECT id FROM users WHERE name = %s LIMIT 1
        """, (username,))
    answer = cur.fetchall()

    if len(answer):
        return answer[0][0]
    return None

##  Retrieves a (non specific) comment made by user_id
#
#   @param cur Cursor for the postgres connection
#   @param user_id Id of the user
#   @return tuple (project_id, author_id, target_id, action, target_type) identifiers of a comment made by user
#
#   @error Throws a RuntimeError if the user never did any comment
def get_user_random_comment(cur, user_id):
    cur.execute("""
        SELECT project_id, author_id, target_id, action, target_type FROM events WHERE author_id = %s AND action = 6 LIMIT 1
        """, (user_id,))

    answer = cur.fetchone()

    if answer is None:
        raise RuntimeError("ERROR: User " + user_id + "does not have any comment. Please create one first")

    return answer

##  Inserts an event in the database. You need to commit the changes on the connection afterwards
#
#   @param cur Cursor for the postgres connection
#   @param project_id Id of the project the event is on
#   @param author_id Id of the author of the event
#   @param target_id Id of the specifics linked to the action (id of the comment on the project for example)
#   @param action Number linked to the type of the action (6 is a comment)
#   @param target_type Type of the event
#   @param date Date of the event
def insert_event(cur, project_id, author_id, target_id, action, target_type, date):
    insert_query = "INSERT INTO events (project_id, author_id, target_id, action, target_type, created_at, updated_at) VALUES %s"
    data = [(project_id, author_id, target_id, action, target_type, date, CREATED_AT)] * 30

    psycopg2.extras.execute_values (
            cur, insert_query, data, template=None, page_size=100
            )

##  Get the id of the last event in the database
#
#   @param cur Cursor for the postgres connection
#   @return int Last id in the database
def get_last_id(cur):
    cur.execute("""
        SELECT id FROM events ORDER BY id DESC LIMIT 1
        """)
    answer = cur.fetchone()
    return answer[0]

##  Delete a drawing based on its infos
#
#   @param cur Cursor for the postgres connection
#   @param author_id Id of the author of the event
#   @param before_id Id of the last event before you inserted your graph
#   @param after_id Id of the last event inserted for your graph
#   @param project_id Id of the project the event is on
#   @param target_id Id of the specifics linked to the action (id of the comment on the project for example)
#   @param action Number linked to the type of the action (6 is a comment)
#   @param target_type Type of the event
def delete_drawing(cur, author_id, before_id, after_id, project_id, target_id, action, target_type):
    cur.execute("""
        DELETE FROM events WHERE id>%s AND id<=%s AND project_id=%s AND author_id=%s AND target_id=%s AND action=%s AND target_type=%s
        """,(before_id, after_id, project_id, author_id, target_id, action, target_type))
