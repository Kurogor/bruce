import yaml

##  Retrieves a specific section from a yaml configuration file
#
#   @param section Name of the section in the file
#   @param filename Name of the configuration file
#   @return dict of the section in the configuration file
def get_config(section, filename='values.yaml'):
    with open(filename) as f:
        data = yaml.load(f, Loader=yaml.loader.SafeLoader)

    if (section == ''):
        return data
    elif(not (data.get(section) is None)):
        return data.get(section)
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))

